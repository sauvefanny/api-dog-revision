export class Dog {
    id;
    name;
    breed;
    picture;
    available;
    /**
     * 
     * @param {string} name 
     * @param {string} breed 
     * @param {string} picture 
     * @param {boolean} available
     * @param {number} id 
     */
    constructor(name,breed,picture,available = true, id=null) {
        this.name = name;
        this.breed = breed;
        this.picture = picture;
        this.available = available;
        this.id = id;
    }
}