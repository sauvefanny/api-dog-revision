DROP TABLE IF EXISTS dog;
CREATE TABLE dog(
    id INTEGER AUTO_INCREMENT PRIMARY KEY ,
    name VARCHAR(255) NOT NULL,
    breed VARCHAR (255),
    picture VARCHAR (255),
    available TINYINT NOT NULL DEFAULT(1)
);

INSERT INTO dog (name,breed,picture) VALUES 
('Fido', 'corgi', 'https://cdn.pixabay.com/photo/2019/08/19/07/45/dog-4415649_960_720.jpg'),
('Rex', 'Samoyed', 'https://cdn.pixabay.com/photo/2017/06/14/00/59/samoyed-2400687_960_720.jpg'),
('Doggo', 'Capybara', 'https://cdn.pixabay.com/photo/2014/07/31/22/51/capybara-407069_960_720.jpg');